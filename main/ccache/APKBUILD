# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=ccache
pkgver=4.10.1
pkgrel=0
pkgdesc="fast C/C++ compiler cache"
url="https://ccache.dev/"
arch="all"
license="GPL-3.0-or-later"
makedepends="
	asciidoctor
	cmake
	doctest-dev
	hiredis-dev
	linux-headers
	perl
	samurai
	xxhash-dev
	zstd-dev
	"
checkdepends="bash util-linux-misc python3"
subpackages="$pkgname-doc"
source="https://github.com/ccache/ccache/releases/download/v$pkgver/ccache-$pkgver.tar.xz
	ioctl.patch
	"

prepare() {
	default_prepare

	# The riscv64 builder seems to have setgid bit set,
	# which causes tests 2, 22, and 34 to fail.
	chmod -v -s .
}

build() {
	cmake -B build -G Ninja \
		-DCCACHE_DEV_MODE=OFF \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

check() {
	ctest --output-on-failure --test-dir build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	local link=
	mkdir -p "$pkgdir"/usr/lib/ccache/bin

	for link in cc gcc g++ cpp c++ $CHOST-cc $CHOST-gcc \
		$CHOST-g++ $CHOST-c++ c89 c99; do
		ln -sf ../../../bin/ccache "$pkgdir"/usr/lib/ccache/bin/$link
	done
}

sha512sums="
98ad98ddc2d05d5779e507190df838317b41bc9b196aa20a43bba21700712b671c581ce7379094625dc6411e720cc7f5cb03d4d3b1df925cb5ce614363dea373  ccache-4.10.1.tar.xz
9201b2c160b5f544ee985c372b9ab25dda1612d94e1018fcc4900bce7ee4741322a257b4799e0d2f6f1b9ccc05b4aa3aa227778f36399de39d4a5d7b64ea9863  ioctl.patch
"
