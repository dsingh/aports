# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=print-manager
pkgver=6.1.2
pkgrel=0
# armhf blocked by qt6-qtdeclarative
arch="all !armhf"
url="https://www.kde.org/applications/utilities/"
pkgdesc="A tool for managing print jobs and printers"
license="GPL-2.0-or-later"
makedepends="
	cups-dev
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kirigami-addons-dev
	knotifications-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	libplasma-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/utilities/print-manager.git"
source="https://download.kde.org/stable/plasma/$pkgver/print-manager-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
dbfd0c7a11e410fc84617bb0c0cd5b648e737765021cf11692b841b1076b47391e39e133213d961783daa83ab87f13b5356c01d7493c9fa480d94fd6eeef4ea7  print-manager-6.1.2.tar.xz
"
