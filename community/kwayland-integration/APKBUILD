# Contributor: Bhushan Shah <bshah@kde.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kwayland-integration
pkgver=6.1.2
pkgrel=0
pkgdesc="KWayland integration"
url="https://kde.org/plasma-desktop/"
arch="all !armhf" # armhf blocked by extra-cmake-modules
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="kglobalaccel5"
makedepends="
	extra-cmake-modules
	kguiaddons5-dev
	kidletime5-dev
	kwayland5-dev
	kwindowsystem5-dev
	qt5-qtbase-dev
	samurai
	wayland-protocols
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/kwayland-integration.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/kwayland-integration-$pkgver.tar.xz"
options="!check" # Broken

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
38360838a112ed20a235cac79f7647cdfffc89f4e61ff72fa8572cb5cdc8cb5ba1ff10d409079c004a97e70e2fc31f2de8ae84b6bb35117b2a183e890e8e56ae  kwayland-integration-6.1.2.tar.xz
"
