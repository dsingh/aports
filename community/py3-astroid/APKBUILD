# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: fossdd <fossdd@pwned.life>
pkgname=py3-astroid
pkgver=3.2.3
pkgrel=0
pkgdesc="A new abstract syntax tree from Python's ast"
url="https://pylint.pycqa.org/projects/astroid/en/latest/"
arch="noarch"
license="LGPL-2.1-or-later"
depends="python3 py3-lazy-object-proxy py3-wrapt"
replaces="py-logilab-astng"
makedepends="
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest py3-typing-extensions"
subpackages="$pkgname-pyc"
source="py3-astroid-$pkgver.tar.gz::https://github.com/PyCQA/astroid/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/astroid-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	# pip test
	pytest -k 'not test_no_user_warning'
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/astroid-*.whl
}

sha512sums="
d9e8b85401598c0ffd3a27c37c4133bc9bba8436cdeb3dadc9a2337ebbf731c062451cdc5c9cf72a768ecf98253051ac012cfdb08084009a9c4a9043acfeabbc  py3-astroid-3.2.3.tar.gz
"
