# Contributor: Maxim Karasev <mxkrsv@disroot.org>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=py3-sympy
pkgver=1.13.0
pkgrel=0
pkgdesc="Computer algebra system written in pure Python"
url="https://www.sympy.org/"
arch="noarch"
license="BSD-3-Clause"
depends="py3-mpmath"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="
	py3-hypothesis
	py3-pytest-rerunfailures
	py3-pytest-timeout
	py3-pytest-xdist
	"
subpackages="$pkgname-doc $pkgname-pyc"
source="https://github.com/sympy/sympy/archive/sympy-$pkgver/py3-sympy-$pkgver.tar.gz"
builddir="$srcdir/sympy-sympy-$pkgver"
# DO NOT MERGE with tests enabled: they are fine in CI,
# but get stuck and never complete on the builders.
options="!check"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	local allow_fail='no'
	case "$CARCH" in
		riscv64) allow_fail='yes' ;;
	esac

	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl

	timeout 2h .testenv/bin/python3 -m pytest \
		-n auto \
		--maxprocesses 24 \
		--timeout 600 \
		--reruns 3 \
	|| [ "$allow_fail" = yes ]
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl

	find "$pkgdir" -type d -name "tests" -exec rm -r {} +
}

sha512sums="
864e0be68bf0c63187d7eeaaa9455a3cea27718e5cf75105e411c7bfadc671928544ec49babe4a513630e8b5d6d45a63c19fc9551ca4ef7c7a0d84c4d62a6e2c  py3-sympy-1.13.0.tar.gz
"
