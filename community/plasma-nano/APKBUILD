# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-nano
pkgver=6.1.2
pkgrel=0
pkgdesc="A minimal Plasma shell package intended for embedded devices"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/kde/plasma-nano"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="qt6-qtvirtualkeyboard"
makedepends="
	extra-cmake-modules
	kitemmodels-dev
	kwayland-dev
	kwindowsystem-dev
	libplasma-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/plasma-nano.git"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-nano-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f0ee197dfca6d5361a283832424b20d44399f48d99c8cf3ddd62119295c8df63275ee181a7a20d2e2b07b6e31c9b7e1e30c8016c7d5ed12e1006d78a803231b1  plasma-nano-6.1.2.tar.xz
"
