# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-pycircos
pkgver=0.3.0
pkgrel=3
pkgdesc="Circular genome visualization package"
url="https://github.com/ponnhide/pyCircos"
arch="noarch !s390x"
license="GPL-3.0-or-later"
depends="
	python3
	py3-biopython
	py3-matplotlib
	py3-requests
	"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
subpackages="$pkgname-pyc"
source="https://github.com/ponnhide/pyCircos/archive/v$pkgver/pycircos-$pkgver.tar.gz"
builddir="$srcdir/pyCircos-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

# no tests provided | use smoke test
check() {
	PYTHONPATH=build/lib python3 -c "from pycircos import *"
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}


sha512sums="
38489209594703589f55c1563416980a3462e94b3c0f805818536bd5b3eb2989d273a8b75e5d26fa37619c81ccec2bddb815ba63d3f4e6cc7eef4b1c56c4da22  pycircos-0.3.0.tar.gz
"
